## 关于JSONModel生成器和其使用方法

***

有些项目中网络读取信息，先解析json转换模型使用比较安全而且快速稳定的做法。

这个工具帮你把json字符串转换模型。导入的框架有 JSONModel 和 MJExtension

JSONModel : [https://github.com/icanzilb/JSONModel](https://github.com/icanzilb/JSONModel)  
提供很多种方法，使用非常方便。

MJExtension : [https://github.com/CoderMJLee/MJExtension](https://github.com/CoderMJLee/MJExtension)  
使用Objictive-C原型模型来处理。

1. 先走侧输入准备好的Json字符串。
2. 按生成键生成预备模型。
3. 重命名预备模型
4. 保存生成好的文件导入XCode导入即可。

***

希望广大朋友们支持

设计：Ablikim Ahmat  
邮箱： xabstan@live.com  
QQ： 1456657135  
微信：xabstan